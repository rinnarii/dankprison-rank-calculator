
function calculate()
{
    var prestigeStart, prestigeEnd, prestigeDifference;
    prestigeStart = parseInt(document.getElementById("prestige_start").value);
    prestigeEnd = parseInt(document.getElementById("prestige_end").value);
    
    prestigeDifference = (prestigeEnd - prestigeStart) * 27;
    
    var masterStart, masterEnd, masterDifference;
    masterStart = parseInt(document.getElementById("master_start").value);
    masterEnd = parseInt(document.getElementById("master_end").value);
    
    masterDifference = (masterEnd - masterStart) * 2727;
    
    var championStart, championEnd, championDifference;
    championStart = parseInt(document.getElementById("champion_start").value);
    championEnd = parseInt(document.getElementById("champion_end").value);
    
    championDifference = (championEnd - championStart) * 275427;
    
    var knightStart, knightEnd, knightDifference;
    knightStart = parseInt(document.getElementById("knight_start").value);
    knightEnd = parseInt(document.getElementById("knight_end").value);
    
    knightDifference = (knightEnd - knightStart) * 27818127;
    
    var rankStart, rankStartNumber, rankEnd, rankEndNumber;
    rankStart = document.getElementById("rank_start").value;
    rankEnd = document.getElementById("rank_end").value;
    
    rankStartNumber = rankNumber(rankStart);
    rankEndNumber = rankNumber(rankEnd);
    
    function rankNumber(rank) {
		if (rank.toLowerCase() === "free") {
			return 27;
		}
		else {
			return parseInt(rank, 36) - 9;
		}
	}
    var rankDifference = rankEndNumber - rankStartNumber;
    
    var totalDifference = Math.abs(rankDifference + prestigeDifference + masterDifference + championDifference + knightDifference);
    document.getElementById("result").innerHTML = totalDifference;
}
